import { FBRemote } from 'remote/remote';


export const Profile = {

	state:{
		loggedIn:false,
		userid:null,
		username:null
	},

	loggedOut(){
		this.state.loggedIn = false;
	},

	loggedIn(){
		this.state.loggedIn=true;
	},

	logout(){
		FBRemote.logout();
	},

	userModules(uid) {
		return FBRemote.userModules(uid);
	},

	/**
	 *
	 * @param {UserModule} m
	 * @returns {UploadTask}
	 */
	saveModule(m){
		return FBRemote.saveModule( m );
	},

	/**
	 *
	 * @param {*} m
	 * @returns {Promise<object>}
	 */
	deleteModule(m){

		return FBRemote.tryDeleteModule( m );

	},

	getState(){return this.state;}

}

FBRemote.on( 'logout', Profile.loggedOut, Profile );
FBRemote.on( 'login', Profile.loggedIn, Profile );