
/**
 * Identifier based on current time and a random suffix that is extremely unlikely to be duplicated.
 * @param {string} prefix
 */
export const TimeId = (prefix)=>( prefix + Date.now().toString(36).slice(3) + (8192*Math.random()).toString(36) );